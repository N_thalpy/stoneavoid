﻿using Tri.SystemComponent;
using System;
using System.Diagnostics;
using System.Reflection;

namespace Tri
{
    [UnitTest]
    public struct Second
    {
        /// <summary>
        /// value of second
        /// </summary>
        private float InnerValue;

        public static Second Zero
        {
            get
            {
                return (Second)0f;
            }
        }

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="value"></param>
        private Second(float value)
        {
            this.InnerValue = value;
        }

        #region Type Casting
        public static explicit operator Second(float time)
        {
            return new Second(time);
        }
        public static implicit operator float (Second time)
        {
            return time.InnerValue;
        }
        #endregion

        #region Operator overloading
        public static Second operator +(Second lhs, Second rhs)
        {
            return (Second)(lhs.InnerValue + rhs.InnerValue);
        }
        public static Second operator -(Second lhs, Second rhs)
        {
            return (Second)(lhs.InnerValue - rhs.InnerValue);
        }
        public static Second operator *(Second lhs, float rhs)
        {
            return (Second)(lhs.InnerValue * rhs);
        }
        public static Second operator *(float lhs, Second rhs)
        {
            return (Second)(rhs.InnerValue * lhs);
        }
        public static Second operator /(Second lhs, float rhs)
        {
            return (Second)(lhs.InnerValue / rhs);
        }
        public static Second operator -(Second lhs)
        {
            return new Second(-lhs.InnerValue);
        }
        #endregion

        #region UnitTest
        public static void UnitTest()
        {
            Random rd = new Random();
            Second lhs = rd.NextSecond(-(Second)10, (Second)10);
            Second rhs = rd.NextSecond(-(Second)10, (Second)10);
            float f = rd.NextFloat(-100, 100);
            
            Debug.Assert((lhs + rhs).InnerValue == (float)(lhs.InnerValue + rhs.InnerValue));
            Debug.Assert((lhs - rhs).InnerValue == (float)(lhs.InnerValue - rhs.InnerValue));
            Debug.Assert((f * lhs).InnerValue == (float)(f * lhs.InnerValue));
            Debug.Assert((lhs * f).InnerValue == (float)(f * lhs.InnerValue));
            Debug.Assert((lhs / f).InnerValue == (float)(lhs.InnerValue / f));
        }
        #endregion
    }
}
