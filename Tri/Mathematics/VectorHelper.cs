﻿using OpenTK;

namespace Tri.Mathematics
{
    public static class VectorHelper
    {
        public static Degree Angle(Vector2 vec)
        {
            float cos = vec.X / vec.Length;
            Degree acos = Mathf.Acos(cos);

            if (vec.Y < 0)
                return (Degree)360 - acos;
            else
                return acos;
        }
    }
}
