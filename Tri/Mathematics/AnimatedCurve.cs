﻿using System.Collections.Generic;
using System.Linq;

namespace Tri.Mathematics
{
    /// <summary>
    /// Warning: AnimatedCurve<T> uses dynamic keyword because not optimized yet.
    /// TODO: Optimize
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class AnimatedCurve<T> where T : struct
    {
        /// <summary>
        /// List of keyframe
        /// </summary>
        private List<KeyFrame<T>> keyFrame;

        /// <summary>
        /// .ctor
        /// </summary>
        public AnimatedCurve()
        {
            keyFrame = new List<KeyFrame<T>>();
        }

        /// <summary>
        /// Add keyframe
        /// </summary>
        /// <param name="time"></param>
        /// <param name="value"></param>
        public void AddKeyFrame(Second time, T value)
        {
            keyFrame.Add(new KeyFrame<T>(time, value));
            keyFrame.OrderBy(_ => _.Time);
        }

        /// <summary>
        /// Get next value of keyframe
        /// </summary>
        /// <returns></returns>
        public T GetValue(Second sec)
        {
            int index = 0;
            foreach (KeyFrame<T> frame in keyFrame)
            {
                if (frame.Time > sec)
                {
                    index--;
                    break;
                }

                index++;
            }

            if (index == -1)
                return keyFrame[0].Value;
            if (index == keyFrame.Count)
                return keyFrame[keyFrame.Count - 1].Value;

            var curr = keyFrame[index];
            var next = keyFrame[index + 1];
            return GetInterpolatedValue(curr.Value, next.Value, (sec - curr.Time) / (next.Time - curr.Time));
        }
        /// <summary>
        /// Get delta of keyframe
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public T GetDelta(Second from, Second to)
        {
            dynamic dfrom = GetValue(from);
            dynamic dto = GetValue(to);

            return dto - dfrom;
        }

        /// <summary>
        /// Get interpolated value between lhs and rhs
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <param name="weight"></param>
        /// <returns></returns>
        private T GetInterpolatedValue(T lhs, T rhs, float weight)
        {
            dynamic dlhs = lhs;
            dynamic drhs = rhs;

            return dlhs * (1 - weight) + drhs * weight;
        }
    }
}
