﻿using System;

namespace Tri.Mathematics
{
    public static class Mathf
    {
        public const float Pi = (float)Math.PI;

        public static float Cos(Degree degree)
        {
            return (float)Math.Cos(degree.InnerValue * Pi / 180);
        }
        public static Degree Acos(float cos)
        {
            return (Degree)Math.Acos(cos) / Pi * 180;
        }
        public static float Sin(Degree degree)
        {
            return (float)Math.Sin(degree.InnerValue * Pi / 180);
        }
        public static Degree Asin(float sin)
        {
            return (Degree)Math.Asin(sin) / Pi * 180;
        }

        public static float Min(float lhs, float rhs)
        {
            return lhs < rhs ? lhs : rhs;
        }
        public static float Max(float lhs, float rhs)
        {
            return lhs > rhs ? lhs : rhs;
        }
    }
}
