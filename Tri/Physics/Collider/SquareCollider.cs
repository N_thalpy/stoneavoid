﻿using OpenTK;
using System;
using System.Collections.Generic;
using Tri.Mathematics;

namespace Tri.Physics.Collider
{
    public class SquareCollider : PolygonCollider
    {
        private bool isPolygonDirty;

        #region public Variable
        /// <summary>
        /// Center of square
        /// </summary>
        public Vector2 Center
        {
            get;
            private set;
        }
        /// <summary>
        /// Width of square
        /// </summary>
        public Vector2 Size
        {
            get;
            private set;
        }
        #endregion

        #region .ctor
        /// <summary>
        /// .ctor
        /// </summary>
        public SquareCollider(Vector2 center, Vector2 size, Degree angle)
            : base(new Polygon())
        {
            Center = center;
            Size = size;
            Angle = angle;
            
            CalculatePolygon();
        }
        #endregion

        /// <summary>
        /// Is this collider collided w/ opposite collider?
        /// </summary>
        /// <param name="opposite">Opposite collider</param>
        /// <returns></returns>
        public override Boolean IsCollided(ColliderBase opposite)
        {
            if (isPolygonDirty == true)
            {
                CalculatePolygon();
                isPolygonDirty = false;
            }
            if (opposite is SquareCollider)
            {
                SquareCollider sc = opposite as SquareCollider;
                if (sc.isPolygonDirty == true)
                {
                    sc.CalculatePolygon();
                    sc.isPolygonDirty = false;
                }
            }

            return base.IsCollided(opposite);
        }
        /// <summary>
        /// Re-calculate polygon
        /// </summary>
        private void CalculatePolygon()
        {
            Vector2 axisx = Vector2.UnitX * Mathf.Cos(Angle) - Vector2.UnitY * Mathf.Sin(Angle);
            Vector2 axisy = Vector2.UnitX * Mathf.Sin(Angle) + Vector2.UnitY * Mathf.Cos(Angle);

            GenerateConvexPolygon(new List<Vector2>(new Vector2[]
            {
                Position + Center - axisx * Size.X / 2 - axisy * Size.Y / 2,
                Position + Center + axisx * Size.X / 2 - axisy * Size.Y / 2,
                Position + Center + axisx * Size.X / 2 + axisy * Size.Y / 2,
                Position + Center - axisx * Size.X / 2 + axisy * Size.Y / 2,
                PrevPosition + Center - axisx * Size.X / 2 - axisy * Size.Y / 2,
                PrevPosition + Center + axisx * Size.X / 2 - axisy * Size.Y / 2,
                PrevPosition + Center + axisx * Size.X / 2 + axisy * Size.Y / 2,
                PrevPosition + Center - axisx * Size.X / 2 + axisy * Size.Y / 2,
            }));
        }

        public override void Update(Second deltaTime)
        {
            base.Update(deltaTime);
            CalculatePolygon();
        }
    }
}
