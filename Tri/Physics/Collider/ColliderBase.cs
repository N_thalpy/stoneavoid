﻿using OpenTK;
using System;

namespace Tri.Physics.Collider
{
    public abstract class ColliderBase
    {
        /// <summary>
        /// Position of collider at previous frame.
        /// </summary>
        protected Vector2 PrevPosition;
        /// <summary>
        /// Position of collider.
        /// </summary>
        public Vector2 Position
        {
            get;
            protected set;
        }
        /// <summary>
        /// Velocity of collider.
        /// </summary>
        public Vector2 Velocity;
        /// <summary>
        /// Angle of collider. ccw.
        /// </summary>
        public Degree Angle
        {
            get;
            protected set;
        }
        public Degree Torque;

        public ColliderBase()
        {
            Position = Vector2.Zero;
            Velocity = Vector2.Zero;
            Angle = Degree.Zero;
            Torque = Degree.Zero;
        }

        public abstract Boolean IsCollided(ColliderBase op);
        public virtual void Update(Second deltaTime)
        {
            PrevPosition = Position;
            Position += Velocity * deltaTime;
            Angle += Torque * deltaTime;
        }
    }
}
