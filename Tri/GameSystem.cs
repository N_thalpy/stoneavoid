﻿using Tri.Coroutine;
using Tri.GameLoop;
using Tri.SystemComponent;
using OpenTK.Audio;
using System;
using System.Reflection;
using Tri.Rendering.Camera;
using OpenTK;

namespace Tri
{
    /// Program.Main    -- calls --> GameSystem.Run
    /// GameSystem.Run  -- calls --> MainForm.Run (which overrides GameWindow.Run)
    /// MainForm.Run    -- calls --> GameSystem.UpdateTick & GameSystem.RenderTick
    public static class GameSystem
    {
        #region public get/private set variables
        public static TriVersion GameVersion
        {
            get;
            private set;
        }
        public static bool IsDebug;

        public static MainForm MainForm
        {
            get;
            private set;
        }
        public static Profiler Profiler
        {
            get;
            private set;
        }
        public static LogSystem LogSystem
        {
            get;
            private set;
        }
        public static AudioContext AudioContext
        {
            get;
            private set;
        }
        public static UITracer UITracer
        {
            get;
            private set;
        }
        public static PolygonTracer PolygonTracer
        {
            get;
            private set;
        }
        public static CoroutineManager CoroutineManager
        {
            get;
            private set;
        }

        public static GameTime UpdateTime
        {
            get;
            private set;
        }
        public static GameTime RenderTime
        {
            get;
            private set;
        }

        public static OrthographicCamera DebugCamera
        {
            get;
            private set;
        }

        public static GameLoopBase CurrentGameLoop
        {
            get;
            private set;
        }
        #endregion

        #region .ctor/init
        /// <summary>
        /// static .ctor
        /// </summary>
        static GameSystem()
        {
            GameVersion = new TriVersion(1, 1, 2);
        }
        /// <summary>
        /// Initializer
        /// </summary>
        public static void Initialize(int width, int height, String name)
        {
            UpdateTime = new GameTime();
            RenderTime = new GameTime();

            MainForm = new MainForm(width, height, name);
            Profiler = new Profiler();
            LogSystem = new LogSystem();
            AudioContext = new AudioContext();
            UITracer = new UITracer();
            PolygonTracer = new PolygonTracer();
            CoroutineManager = new CoroutineManager();

            DebugCamera = new OrthographicCamera(new Vector2(width, height), new Vector2(width / 2, height / 2));

            //Run Unit Test
            Assembly asm = Assembly.GetAssembly(typeof(GameSystem));
            foreach (Type t in asm.GetTypes())
                if (t.GetCustomAttribute(typeof(UnitTestAttribute)) != null)
                {
                    GameSystem.LogSystem.WriteLine(LogType.Info, "Starting Unit Test: {0}... ", t.FullName);
                    t.GetMethod("UnitTest").Invoke(null, null);
                }
        }
        #endregion

        #region Game related
        /// <summary>
        /// Assign Game Loop to loop
        /// </summary>
        /// <param name="gameLoopBase"></param>
        public static void RunGameLoop(GameLoopBase gameLoopBase)
        {
            CurrentGameLoop = gameLoopBase;
            CurrentGameLoop.Initialize();
        }
        /// <summary>
        /// Run game.
        /// </summary>
        public static void Run()
        {
            // Initialize UpdateTime and RenderTime before enter the game
            UpdateTime.Refresh();
            RenderTime.Refresh();

            MainForm.Run();
        }
        #endregion
        #region Tick related
        /// <summary>
        /// Update game. Called for each update tick
        /// </summary>
        public static void UpdateTick()
        {
            UpdateTime.Refresh();
            Second deltaTime = UpdateTime.DeltaTime;

            Profiler.Update(deltaTime);
            CoroutineManager.Update(deltaTime);
            UITracer.Update();
            PolygonTracer.Update();
            InputHelper.Update();

            CurrentGameLoop.Update(deltaTime);
        }
        /// <summary>
        /// Render game. Called for each rendering tick
        /// </summary>
        public static void RenderTick()
        {
            RenderTime.Refresh();
            Second deltaTime = RenderTime.DeltaTime;

            CurrentGameLoop.Render(deltaTime);
            
            Profiler.Render();
            LogSystem.Render();
            UITracer.Render();
            PolygonTracer.Render();
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Dispose Game System
        /// </summary>
        public static void Dispose()
        {
            Profiler.Dispose();
            LogSystem.Dispose();
            AudioContext.Dispose();
        }
        #endregion
    }
}
