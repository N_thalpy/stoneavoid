﻿using Tri.Rendering;
using OpenTK;
using System;
using System.Drawing;
using Tri.Rendering.Renderer;
using OpenTK.Graphics;

namespace Tri.UI
{
    public class Label : View
    {
        private Font font;
        /// <summary>
        /// Font to use
        /// </summary>
        public Font Font
        {
            get
            {
                return font;
            }
            set
            {
                if (font == value)
                    return;

                font = value;
                isTextureDirty = true;
            }
        }

        private String text;
        /// <summary>
        /// Text to use
        /// </summary>
        public String Text
        {
            get
            {
                return text;
            }
            set
            {
                if (text == value)
                    return;

                text = value;
                isTextureDirty = true;
            }
        }

        private Color4 color;
        public Color4 Color
        {
            get
            {
                return color;
            }
            set
            {
                if (color == value)
                    return;

                color = value;
                isTextureDirty = true;                
            }
        }

        /// <summary>
        /// Does texture needs calculation?
        /// </summary>
        private bool isTextureDirty;

        /// <summary>
        /// Inner texture for OpenTK
        /// </summary>
        private Texture tex;

        /// <summary>
        /// .ctor
        /// </summary>
        public Label()
            : base()
        {
            Text = String.Empty;
            isTextureDirty = true;
            UpdateTexture();
        }
        /// <summary>
        /// Create new texture
        /// </summary>
        private void UpdateTexture()
        {
            Graphics g = Graphics.FromHwnd(GameSystem.MainForm.WindowInfo.Handle);
            SizeF stringSize;

            if (String.IsNullOrWhiteSpace(Text) == true)
                stringSize = new SizeF(1, 1);
            else
                stringSize = g.MeasureString(Text, Font);
            
            using (Bitmap bitmap = new Bitmap((int)stringSize.Width, (int)stringSize.Height))
            {
                using (Graphics gfx = Graphics.FromImage(bitmap))
                {
                    PointF pt = new PointF(0.0f, 0.0f);

                    gfx.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                    gfx.Clear(System.Drawing.Color.Transparent);
                    gfx.DrawString(Text, Font, Brushes.White, pt);
                }
                
                if (tex != null)
                    tex.Dispose();
                tex = Texture.CreateFromBitmap(bitmap);
            }

            Size = new Vector2(stringSize.Width, stringSize.Height);
        }

        /// <summary>
        /// Draws texture
        /// </summary>
        protected override void OnRender(ref Matrix4 mat)
        {
            if (isTextureDirty == true)
            {
                UpdateTexture();
                isTextureDirty = false;
            }

            using (BillboardRenderer br = new BillboardRenderer(1))
            {
                br.Begin();
                br.Render(new RenderParameter()
                {
                    Position = new Vector3(Position.X, Position.Y, -Depth),
                    Anchor = Anchor,
                    Size = Size,
                    Angle = Angle,
                    Color = Color,
                });
                br.End(ref mat, tex);
            }
        }

        /// <summary>
        /// Dispose label
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();
            tex.Dispose();
        }
    }
}
