﻿using Tri.SystemComponent;
using OpenTK.Input;
using System;
using System.Collections.Generic;

namespace Tri.UI
{
    public class TextView : Label
    {
        /// <summary>
        /// Is this text view focused?
        /// </summary>
        public bool IsFocused;

        /// <summary>
        /// Dictionary for keys
        /// </summary>
        private static Dictionary<Key, String> KeyDictionary;

        /// <summary>
        /// static .ctor
        /// </summary>
        static TextView()
        {
            KeyDictionary = new Dictionary<Key, string>();

            // A - Z
            for (int ascii = (int)Key.A; ascii <= (int)Key.Z; ascii++)
                KeyDictionary.Add((Key)ascii, ((char)(ascii + 14)).ToString());
            KeyDictionary.Add(Key.Space, " ");
            KeyDictionary.Add(Key.Enter, "\n");
        }
        /// <summary>
        /// .ctor
        /// </summary>
        public TextView() 
            : base()
        {
            IsFocused = false;

            CalculateHitbox();
            this.OnTransformChanged += (s, e) =>
            {
                (s as TextView).CalculateHitbox();
            };
        }

        /// <summary>
        /// Get input and handle text
        /// </summary>
        /// <param name="deltaTime"></param>
        protected override void OnUpdate(Second deltaTime)
        {
            base.OnUpdate(deltaTime);

            if (InputHelper.IsPressed(MouseButton.Left))
                IsFocused = Hitbox.IsPointInside(GameSystem.MainForm.GetMousePosition());

            if (IsFocused == true)
            {
                foreach (Key k in KeyDictionary.Keys)
                    if (InputHelper.IsPressed(k) == true)
                        Text += KeyDictionary[k];

                if (InputHelper.IsPressed(Key.BackSpace))
                    if (Text.Length > 0)
                        Text = Text.Substring(0, Text.Length - 1);
            }
        }
    }
}
