﻿using OpenTK;
using OpenTK.Graphics;
using Tri.Rendering;
using Tri.Rendering.Renderer;
using Tri.SystemComponent;

namespace Tri.UI
{
    public class Sprite : View
    {
        public Texture Texture;

        /// <summary>
        /// .ctor
        /// </summary>
        public Sprite()
            : base()
        {
        }

        /// <summary>
        /// Draws sprite
        /// </summary>
        protected override void OnRender(ref Matrix4 mat)
        {
            if (Size == Vector2.Zero)
                GameSystem.LogSystem.WriteLine(LogType.Warning, "Size of sprite is (0, 0)");
            if (Texture == null)
                GameSystem.LogSystem.WriteLine(LogType.Warning, "Texture of sprite is null");
            else if (Texture.TextureHandle == 0)
                GameSystem.LogSystem.WriteLine(LogType.Error, "Texture is disposed");

            using (BillboardRenderer br = new BillboardRenderer(1))
            {
                br.Begin();
                br.Render(new RenderParameter()
                {
                    Position = new Vector3(Position.X, Position.Y, -Depth),
                    Anchor = Anchor,
                    Size = Size,
                    Angle = Angle,
                    Color = Color4.White,
                });
                br.End(ref mat, Texture);
            }
        }
    }
}
