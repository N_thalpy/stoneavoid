﻿using Tri.SystemComponent;
using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Tri.Crypto.Codec
{
    /// <summary>
    /// Codec using AES
    /// </summary>
    [UnitTest]
    public class AESCodec : CodecBase, IDisposable
    {
        public Aes AesObject
        {
            get;
            private set;
        }

        #region .ctor
        /// <summary>
        /// Generate codec using random key
        /// </summary>
        public AESCodec()
        {
            AesObject = Aes.Create();
        }
        /// <summary>
        /// Generate codec using special key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="initVec"></param>
        public AESCodec(Byte[] key, Byte[] initVec)
        {
            AesObject = Aes.Create();
            AesObject.Key = key;
            AesObject.IV = initVec;
        }
        #endregion

        #region override
        public override Byte[] Encode(Byte[] input)
        {
            ICryptoTransform encryptor = AesObject.CreateEncryptor(AesObject.Key, AesObject.IV);
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (BinaryWriter bwEncrypt = new BinaryWriter(csEncrypt))
                    {
                        bwEncrypt.Write(input);
                    }
                    return msEncrypt.ToArray();
                }
            }
        }
        public override Byte[] Decode(Byte[] input)
        {
            ICryptoTransform decryptor = AesObject.CreateDecryptor(AesObject.Key, AesObject.IV);
            using (MemoryStream msDecrypt = new MemoryStream(input))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {
                        return Encoding.UTF8.GetBytes(srDecrypt.ReadToEnd());
                    }
                }
            }
        }
        #endregion
        #region Dispose
        /// <summary>
        /// Dispose AES Encoder
        /// </summary>
        public void Dispose()
        {
            AesObject.Dispose();
            AesObject = null;
        }
        #endregion

        #region Unit Test
        public static void UnitTest()
        {
            String testString = "123123asdasd";
            using (AESCodec codec = new AESCodec())
            {
                Byte[] input = Encoding.UTF8.GetBytes(testString);
                Byte[] encoded = codec.Encode(input);
                Byte[] decoded = codec.Decode(encoded);

                Debug.Assert(testString == Encoding.UTF8.GetString(decoded));
            }
        }
        #endregion
    }
}
