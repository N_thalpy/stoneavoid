﻿using Tri.Crypto.Codec;
using Tri.SystemComponent;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Tri.Crypto
{
    [UnitTest]
    public class SecretFileStream : FileStream
    {
        public CodecBase Codec
        {
            get;
            private set;
        }
        
        #region .ctor
        public SecretFileStream(String path, FileMode mode, CodecBase codec) 
            : base(path, mode)
        {
            Codec = codec;

            Position = 0;
            length = ReadAll().Length;
        }
        #endregion

        #region override
        private Int64 length;
        public override Int64 Length
        {
            get
            {
                return length;
            }
        }
        private Int64 position;
        public override Int64 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }
        #endregion

        #region Helper
        private Byte[] ReadAll()
        {
            Byte[] total = new Byte[base.Length];
            base.Read(total, 0, total.Length);
            base.Position = 0;

            return Codec.Decode(total);
        }
        #endregion
        #region Write
        /// <summary>
        /// Write encoded byte array to stream.
        /// Warning: Re-writing will costs VERY MUCH
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="index"></param>
        /// <param name="count"></param>
        public override void Write(Byte[] buffer, Int32 index, Int32 count)
        {
            if (CanWrite && CanRead)
            {
                Byte[] newBuffer;
                // If stream is not empty
                if (base.Length != 0)
                {
                    Byte[] decoded = ReadAll();
                    newBuffer = new Byte[count - index + decoded.Length];
                    for (int i = 0; i < count - index + decoded.Length; i++)
                    {
                        if (i < decoded.Length)
                            newBuffer[i] = decoded[i];
                        else
                            newBuffer[i] = buffer[index + i - decoded.Length];
                    }
                }
                // If stream is empty
                else
                {
                    newBuffer = new Byte[count - index];
                    for (int i = 0; i < count - index; i++)
                        newBuffer[i] = buffer[index + i];
                }

                Byte[] encoded = Codec.Encode(newBuffer);
                base.Write(encoded, 0, encoded.Length);
                base.Position = 0;
                base.Flush();

                this.length += count;
            }
            else
            {
                throw new IOException("Stream is not both readable and writable");
            }
        }
        public override void WriteByte(Byte value)
        {
            throw new NotImplementedException();
        }
        #endregion
        #region Read
        /// <summary>
        /// Read decoded byte array from stream.
        /// Warning: Each reading will cost VERY MUCH
        /// </summary>
        /// <param name="array"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public override Int32 Read(Byte[] array, Int32 offset, Int32 count)
        {
            if (CanWrite && CanRead)
            {
                Byte[] decoded = ReadAll();
                for (int i = 0; i < array.Length; i++)
                    array[i] = 0;

                int realCount = (int)Math.Min((long)count, length - position);
                for (int i = 0; i < realCount; i++)
                    array[i] = decoded[i + offset + Position];

                Position += realCount;
                return realCount;
            }
            else
            {
                throw new IOException("Stream is not both readable and writable");
            }
        }
        public override Int32 ReadByte()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Unit Test
        public static void UnitTest()
        {
            #region Test#1; Simple read/write
            String path = "__tmp.dat";
            AESCodec aesc = new AESCodec();
            using (SecretFileStream sfs = new SecretFileStream(path, FileMode.Create, aesc))
            {
                sfs.Write(Encoding.UTF8.GetBytes("test"), 0, 4);
                Debug.Assert(sfs.length == 4);
                sfs.Write(Encoding.UTF8.GetBytes("string"), 0, 6);
                Debug.Assert(sfs.length == 10);

                Byte[] array = new Byte[4];
                sfs.Read(array, 0, 4);
                Debug.Assert(Encoding.UTF8.GetString(array) == "test");

                array = new Byte[6];
                sfs.Read(array, 0, 6);
                Debug.Assert(Encoding.UTF8.GetString(array) == "string");
            }
            #endregion
            #region Test#2; XmlSerialization
            XmlSerializer xs = new XmlSerializer(typeof(List<String>));
            List<String> testStringList = new List<String>();
            testStringList.Add("this");
            testStringList.Add("is");
            testStringList.Add("test");
            testStringList.Add("object");

            using (SecretFileStream sfs = new SecretFileStream(path, FileMode.Create, aesc))
                xs.Serialize(sfs, testStringList);
            using (SecretFileStream sfs = new SecretFileStream(path, FileMode.Open, aesc))
            {
                Byte[] array = new Byte[sfs.length];
                sfs.Read(array, 0, array.Length);
                String tmp = Encoding.UTF8.GetString(array);

                sfs.position = 0;
                List<String> deserialized = xs.Deserialize(sfs) as List<String>;
                for (int i = 0; i < testStringList.Count; i++)
                    Debug.Assert(testStringList[i] == deserialized[i]);
            }
            #endregion

            File.Delete(path);
        }
        #endregion
    }
}
