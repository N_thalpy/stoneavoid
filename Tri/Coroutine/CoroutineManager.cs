﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Tri.Coroutine
{
    public class CoroutineManager
    {
        /// <summary>
        /// List of coroutine
        /// </summary>
        private List<IEnumerator> coroutineList;
        /// <summary>
        /// Coroutines in remove candidate
        /// </summary>
        private List<IEnumerator> removeCandidate;

        /// <summary>
        /// Dictionary to find coroutine.
        /// K = handler, V = coroutine
        /// </summary>
        private Dictionary<int, IEnumerator> coroutineDictionary;
        /// <summary>
        /// Last handler
        /// </summary>
        private int lastHandler;

        /// <summary>
        /// .ctor
        /// </summary>
        public CoroutineManager()
        {
            coroutineList = new List<IEnumerator>();
            removeCandidate = new List<IEnumerator>();
            coroutineDictionary = new Dictionary<int, IEnumerator>();
            lastHandler = 0;
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="deltaTime"></param>
        public void Update(Second deltaTime)
        {
            foreach (IEnumerator ienum in removeCandidate)
            {
                int handler = coroutineDictionary.First(_ => _.Value == ienum).Key;
                coroutineDictionary.Remove(handler);
                coroutineList.Remove(ienum);
            }
            removeCandidate.Clear();

            foreach (IEnumerator ienum in coroutineList)
            {
                Debug.Assert(ienum.Current is CoroutineElement);
                while (true)
                {
                    CoroutineElement ce = (ienum.Current as CoroutineElement);
                    if (ce.Finished == true)
                    {
                        if (ienum.MoveNext() == false)
                        {
                            removeCandidate.Add(ienum);
                            break;
                        }
                    }
                    else
                    {
                        ce.Update(deltaTime);
                        break;
                    }
                }
            }
        }
        
        /// <summary>
        /// Adds coroutine to list. Returns handler of coroutine
        /// </summary>
        /// <param name="ienum"></param>
        /// <returns>Handler of coroutine</returns>
        public int AddCoroutine(IEnumerator ienum)
        {
            if (ienum.MoveNext() == true)
                coroutineList.Add(ienum);

            lastHandler++;
            coroutineDictionary.Add(lastHandler, ienum);
            return lastHandler;
        }
        /// <summary>
        /// Make coroutine to remove candidated status
        /// </summary>
        /// <param name="handler">Handler of coroutine to remove</param>
        public void RemoveCoroutine(int handler)
        {
            IEnumerator ienum = coroutineDictionary[handler];
            removeCandidate.Add(ienum);
        }
    }
}
