﻿using System;

namespace Tri.Coroutine
{
    public class WaitForSecond : CoroutineElement
    {
        /// <summary>
        /// Is this element passable?
        /// </summary>
        public override Boolean Finished
        {
            get
            {
                return timeRemain <= Second.Zero;
            }
        }
        /// <summary>
        /// Time to remain
        /// </summary>
        private Second timeRemain;

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="time"></param>
        public WaitForSecond(Second time)
        {
            timeRemain = time;
        }
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="deltaTime"></param>
        public override void Update(Second deltaTime)
        {
            timeRemain -= deltaTime;
        }
    }
}
