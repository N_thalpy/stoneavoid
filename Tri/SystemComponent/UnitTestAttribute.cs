﻿using System;

namespace Tri.SystemComponent
{
    /// <summary>
    /// Attribute for unit test.
    /// MUST IMPLEMENT "public static void UnitTest()" when this attribute is used,
    /// and this function MUST HALT when problem is detected.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false, Inherited = false)]
    public class UnitTestAttribute : Attribute
    {
    }
}
