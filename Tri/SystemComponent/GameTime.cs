﻿using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Tri.SystemComponent
{
    /// <summary>
    /// This class has extracted from Cosmos,
    /// TODO : Not analysed yet.
    /// maybe using WinAPI Functions to make better precision
    /// </summary>
    public class GameTime
    {
        #region dll imports
        [DllImport("kernel32.dll", CallingConvention = CallingConvention.Winapi)]
        private static extern bool QueryPerformanceFrequency(out long frequency);

        [DllImport("kernel32.dll", CallingConvention = CallingConvention.Winapi)]
        private static extern bool QueryPerformanceCounter(out long counter);
        #endregion

        #region public Variables
        /// <summary>
        /// Delta time.
        /// </summary>
        public Second DeltaTime
        {
            get;
            private set;
        }
        /// <summary>
        /// Current frame count
        /// </summary>
        public long Frame
        {
            get;
            private set;
        }
        #endregion
        #region private Variables
        private static long frequency;
        public static long Frequency
        {
            get
            {
                return frequency;
            }
            private set
            {
                if (value == frequency)
                    return;

                frequency = value;
            }
        }

        private long startTick;
        private long currentTick;
        private long oldTick;
        #endregion

        #region .ctor
        /// <summary>
        /// static .ctor
        /// </summary>
        static GameTime()
        {
            QueryPerformanceFrequency(out frequency);
        }
        /// <summary>
        /// .ctor
        /// </summary>
        public GameTime()
        {
            QueryPerformanceCounter(out currentTick);
            startTick = currentTick;
            oldTick = currentTick;
            Frame = 0;
        }
        #endregion
        
        /// <summary>
        /// Reset GameTime
        /// </summary>
        public void Reset()
        {
            QueryPerformanceCounter(out currentTick);
            startTick = currentTick;
            oldTick = currentTick;
            Frame = 0;
        }
        /// <summary>
        /// Update GameTime
        /// </summary>
        public void Refresh()
        {
            oldTick = currentTick;
            QueryPerformanceCounter(out currentTick);

            Debug.Assert(oldTick < currentTick);
            DeltaTime = (Second)((float)(currentTick - oldTick) / Frequency);
            Frame++;
        }
    }
}
