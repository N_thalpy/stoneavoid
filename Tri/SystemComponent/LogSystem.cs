﻿using Tri.UI;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Tri.SystemComponent
{
    public enum LogType
    {
        None,
        Info,
        Warning,
        Error,
    }
    public class LogSystem : IDisposable
    {
        /// <summary>
        /// Max line number of log
        /// </summary>
        private const int MaxLineNumber = 20;

        /// <summary>
        /// List of log
        /// </summary>
        private List<String> logList;

        /// <summary>
        /// Index of log which caret exists
        /// </summary>
        private int logIndex;

        /// <summary>
        /// TextView to display log
        /// </summary>
        private Label label;

        /// <summary>
        /// .ctor
        /// </summary>
        public LogSystem()
        {
            logList = new List<String>();
            logList.Add(String.Empty);
            logIndex = 0;

            label = new Label()
            {
                Position = new Vector2(0, GameSystem.MainForm.Height),
                Anchor = new Vector2(0, 1),
                Depth = 0,
                Font = new Font("Arial.ttf", 12, FontStyle.Regular),
                Color = Color.White,
            };
            WriteLine(LogType.Info, "LogSystem Activated");
        }

        #region Write / WriteLine
        /// <summary>
        /// Writes log and append carrage return
        /// </summary>
        /// <param name="type"></param>
        /// <param name="obj"></param>
        public void WriteLine(LogType type, Object obj)
        {
            WriteLine(type, obj.ToString());
        }
        /// <summary>
        /// Writes log and append carrage return
        /// </summary>
        /// <param name="str"></param>
        /// <param name="obj"></param>
        public void WriteLine(LogType type, String str, params Object[] args)
        {
            lock (logList)
            {
                String typechar = String.Empty;
                switch (type)
                {
                    case LogType.Error:
                        typechar = "e";
                        break;

                    case LogType.Info:
                        typechar = "i";
                        break;

                    case LogType.Warning:
                        typechar = "w";
                        break;

                    case LogType.None:
                        typechar = String.Empty;
                        break;
                }

                logList[logIndex] += GameSystem.UpdateTime.Frame + "F" + typechar + " " + String.Format(str, args);
                logIndex++;
                logList.Add(String.Empty);

                StringBuilder sb = new StringBuilder();
                int idx = logList.Count < MaxLineNumber ? 0 : logList.Count - MaxLineNumber;
                for (; idx < logList.Count; idx++)
                    sb.AppendLine(logList[idx]);

                label.Text = sb.ToString();
            }
        }
        #endregion

        /// <summary>
        /// Renders log
        /// </summary>
        public void Render()
        {
            if (GameSystem.IsDebug)
                label.Render(ref GameSystem.DebugCamera.Matrix);
        }

        /// <summary>
        /// Dispose log system
        /// </summary>
        public void Dispose()
        {
            label.Dispose();
        }
    }
}
