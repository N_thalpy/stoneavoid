﻿using Tri.UI;
using OpenTK;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using Tri.Rendering;

namespace Tri.SystemComponent
{
    public class Profiler : IDisposable
    {
        /// <summary>
        /// TextView to display profiled data
        /// </summary>
        private Label label;

        /// <summary>
        /// Last deltaTime of update tick
        /// </summary>
        private Second lastDeltaTime;

        /// <summary>
        /// Max recommended memory usage.
        /// SHOULD check memory leakage or wrong optimization when memory usage is over this value.
        /// </summary>
        private const long WarningMemoryUsage = 1000 * 1000 * 1000;

        /// <summary>
        /// StringBuilder (for caching)
        /// </summary>
        private StringBuilder sb;

        /// <summary>
        /// .ctor
        /// </summary>
        public Profiler()
        {
            label = new Label()
            {
                Position = new Vector2(0, 0),
                Anchor = new Vector2(0, 0),
                Depth = 0,
                Font = new Font(FontFamily.GenericSansSerif, 12),
                Color = Color.White
            };
            lastDeltaTime = (Second)0f;
            sb = new StringBuilder();
        }
        
        /// <summary>
        /// Get frame per second
        /// </summary>
        /// <returns>fps</returns>
        public float GetFPS()
        {
            if (lastDeltaTime <= 0f)
                return float.NaN;
            else
                return 1 / lastDeltaTime;
        }
        /// <summary>
        /// Get current memory usage
        /// </summary>
        /// <returns>current memory usage</returns>
        public long GetMemoryUsage()
        {
            long memuse = Process.GetCurrentProcess().PrivateMemorySize64;
            if (memuse > WarningMemoryUsage)
                GameSystem.LogSystem.WriteLine(LogType.Warning, "Memory Usage is {0} now. Should check memory optimization", memuse);

            return memuse;
        }

        /// <summary>
        /// Returns profiling message
        /// </summary>
        /// <returns></returns>
        public String GetProfileMessage()
        {
            sb.Clear();
            sb.AppendLine(String.Format("Tri Library {0}", GameSystem.GameVersion));
            sb.AppendLine(String.Format("FPS: {0:F1}", GetFPS()));
            sb.AppendLine(String.Format("Frame Interval: {0:F2}ms", 1000 * (float)lastDeltaTime));
            sb.AppendLine(String.Format("Memory: {0}MB", GetMemoryUsage() / (1000 * 1000)));
            // sb.AppendLine(String.Format("vert: {0}", RenderHelper.DrawList.Count * 4));

            return sb.ToString();
        }

        /// <summary>
        /// Update profiler
        /// </summary>
        /// <param name="deltaTime"></param>
        public void Update(Second deltaTime)
        {
            lastDeltaTime = deltaTime;
        }
        /// <summary>
        /// Render profiler
        /// </summary>
        public void Render()
        {
            if (GameSystem.IsDebug)
            {
                label.Text = GetProfileMessage();
                label.Render(ref GameSystem.DebugCamera.Matrix);
            }
        }

        /// <summary>
        /// Dispose Profiler
        /// </summary>
        public void Dispose()
        {
            label.Dispose();
        }
    }
}
