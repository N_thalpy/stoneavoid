﻿using System;
using System.Diagnostics;

namespace Tri.SystemComponent
{
    /// <summary>
    /// Version of tri library.
    /// Major.Minor.Patch
    /// </summary>
    public struct TriVersion
    {
        public static int MaxMinor = 255;
        public static int MaxPatch = 255;

        public readonly int MajorVersion;
        public readonly int MinorVersion;
        public readonly int PatchVersion;
        
        public TriVersion(int major, int minor, int patch)
        {
            Debug.Assert(major > 0);
            Debug.Assert(minor < MaxMinor && minor >= 0);
            Debug.Assert(patch < MaxPatch && patch >= 0);

            MajorVersion = major;
            MinorVersion = minor;
            PatchVersion = patch;
        }

        #region operator overloading
        public static bool operator == (TriVersion lhs, TriVersion rhs)
        {
            return lhs.MajorVersion == rhs.MajorVersion &&
                lhs.MinorVersion == rhs.MinorVersion &&
                lhs.PatchVersion == rhs.PatchVersion;
        }
        public static bool operator != (TriVersion lhs, TriVersion rhs)
        {
            return (lhs == rhs) == false;
        }
        public static bool operator < (TriVersion lhs, TriVersion rhs)
        {
            if (lhs.MajorVersion != rhs.MajorVersion)
                return lhs.MajorVersion < rhs.MajorVersion;
            else if (lhs.MinorVersion != rhs.MinorVersion)
                return lhs.MinorVersion < rhs.MinorVersion;
            else
                return lhs.PatchVersion < rhs.PatchVersion;
        }
        public static bool operator > (TriVersion lhs, TriVersion rhs)
        {
            if (lhs.MajorVersion != rhs.MajorVersion)
                return lhs.MajorVersion > rhs.MajorVersion;
            else if (lhs.MinorVersion != rhs.MinorVersion)
                return lhs.MinorVersion > rhs.MinorVersion;
            else
                return lhs.PatchVersion > rhs.PatchVersion;
        }
        #endregion
        #region override
        public override Int32 GetHashCode()
        {
            return MajorVersion * MaxMinor * MaxPatch + MinorVersion * MaxPatch + PatchVersion;
        }
        public override Boolean Equals(Object obj)
        {
            if (obj is TriVersion)
                return (TriVersion)obj == this;
            else
                return false;
        }
        public override String ToString()
        {
            return String.Format("V.{0}.{1}.{2}", MajorVersion, MinorVersion, PatchVersion);
        }
        #endregion
    }
}
