﻿using Tri.UI;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Tri.SystemComponent
{
    public class UITracer
    {
        /// <summary>
        /// Dictionary of tracing views
        /// </summary>
        private Dictionary<View, Label> tracerDic;
 
        /// <summary>
        /// .ctor
        /// </summary>
        public UITracer()
        {
            tracerDic = new Dictionary<View, Label>();
        }

        /// <summary>
        /// Start to trace view
        /// </summary>
        /// <param name="view"></param>
        public void Trace(View view)
        {
            float leftwidth = -view.Anchor.X * view.Size.X;
            float upheight = -view.Anchor.Y * view.Size.Y;
            Vector2 pos = new Vector2(leftwidth, upheight).Rotate(view.Angle) + view.Position;
            
            Label lb = new Label()
            {
                Position = pos,
                Anchor = new Vector2(0, 1),
                Depth = 0,
                Font = new Font(FontFamily.GenericSansSerif, 12),
                Color = Color.White,
                Text = String.IsNullOrWhiteSpace(view.DebugName) ? view.GetType().Name.ToString() : view.DebugName,
            };
            tracerDic.Add(view, lb);
        }
        
        /// <summary>
        /// Update tracers
        /// </summary>
        public void Update()
        {
            foreach (View view in tracerDic.Keys)
            {
                float leftwidth = -view.Anchor.X * view.Size.X;
                float upheight = -(1 - view.Anchor.Y) * view.Size.Y;
                Vector2 pos = new Vector2(leftwidth, upheight).Rotate(view.Angle) + view.Position;
                tracerDic[view].Position = pos;

                String text = null;
                if (String.IsNullOrWhiteSpace(text))
                    text = view.GetType().Name.ToString();
                else
                    text = view.DebugName;
                tracerDic[view].Text = text;
            }
        }
        /// <summary>
        /// Render tracers
        /// </summary>
        public void Render()
        {
            if (GameSystem.IsDebug)
                foreach (View v in tracerDic.Keys)
                    tracerDic[v].Render(ref GameSystem.DebugCamera.Matrix);
        }
    }
}
