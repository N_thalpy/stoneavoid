﻿using Tri.Rendering;
using Tri.SystemComponent;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System;
using System.Drawing;
using Tri.Rendering.Renderer;

namespace Tri
{
    public class MainForm : GameWindow
    {
        /// <summary>
        /// .ctor
        /// </summary>
        public MainForm(int width, int height, String name)
            : base(width, height, new GraphicsMode(32, 24, 0, 8), name, 
                  GameWindowFlags.FixedWindow, DisplayDevice.Default,
                  3, 2, GraphicsContextFlags.ForwardCompatible)
        {
        }

        /// <summary>
        /// Update Objects.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);
            GameSystem.UpdateTick();
        }
        /// <summary>
        /// Renders Objects.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            
            GL.ClearColor(Color4.CornflowerBlue);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Enable(EnableCap.Blend);
            
            GameSystem.RenderTick();

            SwapBuffers();
            GLHelper.CheckGLError();
        }

        /// <summary>
        /// Calculate current mouse position based on MainForm window
        /// </summary>
        /// <returns></returns>
        public Vector2 GetMousePosition()
        {
            MouseState cursor = InputHelper.MouseState;
            Point scrPnt = new Point(cursor.X, cursor.Y);
            Point cliPnt = PointToClient(scrPnt);
            return new Vector2(cliPnt.X, cliPnt.Y);
        }
    }
}
