﻿using OpenTK;
using OpenTK.Graphics;

namespace Tri.Rendering.Renderer
{
    public struct RenderParameter
    {
        public Vector3 Position;
        public Vector2 Anchor;
        public Vector2 Size;
        public Degree Angle;
        public Color4 Color;
        public Texture Texture;
    }
}
