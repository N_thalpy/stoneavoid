﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Diagnostics;
using System.Drawing;
using Tri.Rendering.VertexType;

namespace Tri.Rendering.Renderer
{
    public class BillboardRenderer : RendererBase, IDisposable
    {
        private const int VertexPerQuad = 4;
        private const int ElementPerQuad = 6;

        private VertexBuffer vbo;
        private ElementBuffer ebo;
        private int maxQuad;

        private VertexPositionColorTexture[] vertices;
        private int vPos;
        private int[] elements;
        private int ePos;

        #region .ctor
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="maxQuad">Count of quad</param>
        public BillboardRenderer(int maxQuad)
            : base()
        {
            this.maxQuad = maxQuad;

            vertices = new VertexPositionColorTexture[maxQuad * VertexPerQuad];
            vbo = VertexBuffer.CreateDynamic(vertices, ShaderProvider.SimpleTexture);

            elements = new int[maxQuad * ElementPerQuad];
            // Initialize ebo
            int eidx = 0;
            for (int i = 0; i < maxQuad; i++)
            {
                elements[eidx++] = i * VertexPerQuad + 0;
                elements[eidx++] = i * VertexPerQuad + 2;
                elements[eidx++] = i * VertexPerQuad + 1;
                elements[eidx++] = i * VertexPerQuad + 2;
                elements[eidx++] = i * VertexPerQuad + 1;
                elements[eidx++] = i * VertexPerQuad + 3;
            }
            ebo = ElementBuffer.Create(elements);
        }
        #endregion

        public void Begin()
        {
            Debug.Assert(state == RendererState.Idle);
            state = RendererState.Drawing;

            vPos = 0;
            ePos = 0;
        }
        public override void Render(RenderParameter param)
        {
            Debug.Assert(state == RendererState.Drawing);
            RectangleF textureUV = new RectangleF(0, 0, 1, 1);

            Vector2 tl = new Vector2(-param.Anchor.X * param.Size.X, (1 - param.Anchor.Y) * param.Size.Y);
            Vector2 tr = new Vector2((1 - param.Anchor.X) * param.Size.X, (1 - param.Anchor.Y)  * param.Size.Y);
            Vector2 bl = new Vector2(-param.Anchor.X * param.Size.X, -param.Anchor.Y * param.Size.Y);
            Vector2 br = new Vector2((1 - param.Anchor.X) * param.Size.X, -param.Anchor.Y * param.Size.Y);

            VertexPositionColorTexture vertex;
            vertex.Color = param.Color;

            vertex.TextureUV = textureUV.GetTopLeft();
            vertex.Position = param.Position;
            vertex.Position.X += tl.X;
            vertex.Position.Y += tl.Y;
            vertices[vPos++] = vertex;

            vertex.TextureUV = textureUV.GetTopRight();
            vertex.Position = param.Position;
            vertex.Position.X += tr.X;
            vertex.Position.Y += tr.Y;
            vertices[vPos++] = vertex;

            vertex.TextureUV = textureUV.GetBottomLeft();
            vertex.Position = param.Position;
            vertex.Position.X += bl.X;
            vertex.Position.Y += bl.Y;
            vertices[vPos++] = vertex;

            vertex.TextureUV = textureUV.GetBottomRight();
            vertex.Position = param.Position;
            vertex.Position.X += br.X;
            vertex.Position.Y += br.Y;
            vertices[vPos++] = vertex;

            ePos += 6;
        }
        public void End(ref Matrix4 transform, Texture texture)
        {
            Debug.Assert(state == RendererState.Drawing);
            Debug.Assert(vPos / VertexPerQuad == ePos / ElementPerQuad);
            Debug.Assert(vPos / VertexPerQuad <= maxQuad);

            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            state = RendererState.Idle;
            vbo.Bind(vertices);
            ebo.Bind();
            ShaderProvider.SimpleTexture.Bind(ref transform, texture);
            GLHelper.DrawTriangles(ePos);

            GLHelper.CheckGLError();
        }

        public void Dispose()
        {
            vbo.Dispose();
            ebo.Dispose();
        }
    }
}
