﻿using OpenTK;
using System.Drawing;

namespace Tri.Rendering
{
    public struct TransformTextureColor
    {
        public Vector2 Position;
        public Vector2 Anchor;
        public Vector2 Size;
        public Degree Angle;
        public float Depth;
        public Texture Tex;
        public Color Color;
    }
}
