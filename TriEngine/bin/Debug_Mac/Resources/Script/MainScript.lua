﻿MainScript = {}

function SetTextWithAnim(txt, str)
	local idx
	local len = str:len()
	for idx = 1, len do
		txt:SetText(str:sub(1, idx))
		WaitForFrame(3)
	end
end

function Main()
	sp = SpriteObject(150, 150, 100, 100, 'Image/test.png')
	sp:Show()

	local idx
	local spd = 2
	for idx = 0, 720 do
		sp:SetAngle(idx * spd * 3)
		sp:SetPosition(300 + 150 * math.sin(spd * idx * 3.14 / 180), 300 + 150 * math.cos(spd * idx * 3.14 / 180))
		WaitForFrame(1)
	end

	txt = TextObject(600, 150, 'temp', 20)
	txt:Show()
	txt:SetText('1234 This is test string.!\ncarrage return!')
	WaitForEnter()

	SetTextWithAnim(txt, 'Animation test....')
	WaitForEnter()

	CallScript('MainScript.lua');
	MainScript.Main()
end

function MainScript.Main ()
	sp2 = SpriteObject(150, 300, 100, 100, 'Image/test.png')
	sp2:Show()

	WaitForEnter()

	sp:Hide()
	sp2:Hide()
	CallScript('Subroutine.lua')
	Subroutine.Main()

	Exit()
end