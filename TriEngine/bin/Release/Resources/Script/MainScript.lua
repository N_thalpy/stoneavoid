﻿function Main ()
	sp = SpriteObject (150, 600, 100, 100, 'Image/test.png')
	txt = TextObject (500, 600, 'Arial', 30)
	txt:SetText('will exit at next enter!')

	sp:Show ()
	WaitForEnter ()
	
	sp:Hide ()
	WaitForEnter ()

	sp:Show ()
	txt:Show ()
	WaitForEnter ()

	txt:SetText ('will REALLY exit at next enter!')
	WaitForEnter ()
	
	Exit()
end