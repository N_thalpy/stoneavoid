﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;
using System.Drawing;
using Tri;
using Tri.GameLoop;
using Tri.Physics.Collider;
using Tri.Rendering;
using Tri.Rendering.Camera;
using Tri.SystemComponent;
using Tri.UI;

namespace TriTest.GameLoop
{
    public class TestGameLoop : GameLoopBase
    {
        OrthographicCamera cam;

        Sprite sp1, sp2;
        SquareCollider cc1, cc2;
        Texture tex1, tex2;
        Label lb;

        public TestGameLoop()
            : base("Test Game Loop")
        {
            tex1 = Texture.CreateFromBitmapPath("Resource/test.png");
            tex2 = Texture.CreateFromBitmapPath("Resource/test2.png");
        }

        protected override void OnInitialize()
        {
            cam = new OrthographicCamera(
                new Vector2(GameSystem.MainForm.Width, GameSystem.MainForm.Height), 
                new Vector2(GameSystem.MainForm.Width / 2, GameSystem.MainForm.Height / 2));

            sp1 = new Sprite()
            {
                Position = new Vector2(0, 0),
                Size = new Vector2(100, 100),
                Texture = tex1,
            };
            sp2 = new Sprite()
            {
                Position = new Vector2(101, 100),
                Size = new Vector2(100, 100),
                Texture = tex2,
            };
            lb = new Label()
            {
                Position = new Vector2(200, 200),
                Text = "Test string",
                Font = new Font(FontFamily.GenericSansSerif, 20),
                Color = Color4.White,
            };
            
            cc1 = new SquareCollider(sp1.Position, sp1.Size, sp1.Angle);
            cc2 = new SquareCollider(sp2.Position, sp2.Size, sp2.Angle);
        }

        public override void Update(Second deltaTime)
        {
            Vector2 vel = Vector2.Zero;
            Degree torq = Degree.Zero;
            if (InputHelper.IsDown(Key.Left))
                vel += new Vector2(-1000, 0);
            if (InputHelper.IsDown(Key.Right))
                vel += new Vector2(1000, 0);
            if (InputHelper.IsDown(Key.Up))
                vel += new Vector2(0, 1000);
            if (InputHelper.IsDown(Key.Down))
                vel += new Vector2(0, -1000);
            if (InputHelper.IsDown(Key.Q))
                torq += (Degree)100f;
            if (InputHelper.IsDown(Key.E))
                torq -= (Degree)100f;

            if (cc1.IsCollided(cc2))
                GameSystem.LogSystem.WriteLine(LogType.Info, "collision occured");

            sp1.Position += vel * deltaTime;
            sp1.Angle += torq * deltaTime;
            cc1.Velocity = vel;
            cc1.Torque = torq;

            cc1.Update(deltaTime);
            cc2.Update(deltaTime);
        }
        public override void Render(Second deltaTime)
        {
            sp1.Render(ref cam.Matrix);
            sp2.Render(ref cam.Matrix);
            lb.Render(ref cam.Matrix);
        }
        protected override void OnStop()
        {
            sp1.Dispose();
            sp2.Dispose();
            lb.Dispose();
        }
    }
}
