﻿using Tri;
using TriTest.GameLoop;
using System;

namespace TriTest
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            GameSystem.Initialize(1280, 760, "TriTest");

            GameSystem.IsDebug = true;
            GameSystem.RunGameLoop(new TestGameLoop());
            GameSystem.Run();
            GameSystem.Dispose();
        }
    }
}
