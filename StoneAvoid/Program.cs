﻿using StoneAvoid.GameLoop;
using System;
using Tri;

namespace StoneAvoid
{
    public class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            GameSystem.Initialize(1280, 760, "StoneAvoid!!");

            GameSystem.IsDebug = true;
            GameSystem.RunGameLoop(new MainGameLoop());
            GameSystem.Run();
            GameSystem.Dispose();
        }
    }
}
