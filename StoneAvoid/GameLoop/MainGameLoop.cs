﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;
using StoneAvoid.FieldObject;
using System;
using System.Collections.Generic;
using System.Drawing;
using Tri;
using Tri.GameLoop;
using Tri.Rendering;
using Tri.Rendering.Camera;
using Tri.Rendering.Renderer;
using Tri.SystemComponent;
using Tri.UI;

namespace StoneAvoid.GameLoop
{
    public class MainGameLoop : GameLoopBase
    {
        #region enum
        /// <summary>
        /// 게임의 상황을 나타내는 타입
        /// </summary>
        private enum GameStatus
        {
            /// <summary>
            /// 기본값
            /// </summary>
            None,

            /// <summary>
            /// 초기화 된 상태
            /// </summary>
            Initialized,

            /// <summary>
            /// 플레이 중인 상태
            /// </summary>
            Playing,
            
            /// <summary>
            /// 플레이어가 죽은 상태
            /// </summary>
            Dead,
        };
        #endregion
        #region private variables
        /// <summary>
        /// Orthographic한 카메라
        /// </summary>
        private OrthographicCamera cam;
        /// <summary>
        /// 플레이어 객체
        /// </summary>
        private Player player;
        /// <summary>
        /// 현재 화면에 있는 돌 객체의 리스트
        /// </summary>
        private List<Stone> stoneList;

        /// <summary>
        /// 시작 UI
        /// </summary>
        private Label startLabel;
        /// <summary>
        /// 배경 UI
        /// </summary>
        private Sprite backgroundSprite;

        /// <summary>
        /// 돌이 생성되기까지 필요한 시간
        /// </summary>
        private Second stoneGenCooltime;
        /// <summary>
        /// 돌을 생성하기 위해 시간을 재는 변수
        /// </summary>
        private Second stoneGenTimer;

        /// <summary>
        /// 게임의 현재 상태
        /// </summary>
        private GameStatus status;

        /// <summary>
        /// System.Random 객체
        /// </summary>
        private Random random;
        #endregion

        #region .ctor
        /// <summary>
        /// .ctor.
        /// 각 객체들을 적당히 초기화합니다.
        /// </summary>
        public MainGameLoop()
            : base("MainGameLoop")
        {
            // Orthographic 카메라 초기화
            // Camera viewport/position setting:
            // (0, Height) --- (Width, Height)
            // |                             |
            // |                             |
            // |                             |
            // (0, 0) ------------- (Width, 0)
            cam = new OrthographicCamera(
                new Vector2(GameSystem.MainForm.Width, GameSystem.MainForm.Height),
                new Vector2(GameSystem.MainForm.Width / 2, GameSystem.MainForm.Height / 2));

            // 플레이어 객체 생성
            player = new Player(new Vector2(GameSystem.MainForm.Width / 2, 0));
            // 돌 리스트 생성
            stoneList = new List<Stone>();
            
            // 시작 UI 생성
            // 검은색, sans serif 폰트로 "Press Space to Play!" 를 출력한다.
            startLabel = new Label()
            {
                Position = new Vector2(GameSystem.MainForm.Width / 2, 400),
                Anchor = new Vector2(0.5f, 0.5f),
                Text = "Press Space to Play!",
                Font = new Font(FontFamily.GenericSansSerif, 40),
                Color = Color4.Black,
            };

            // 배경 이미지
            backgroundSprite = new Sprite()
            {
                Position = Vector2.Zero,
                Depth = ZOrder.Background,
                Anchor = Vector2.Zero,
                Texture = Texture.CreateFromBitmapPath("Resources/Background.png"),
                Size = new Vector2(GameSystem.MainForm.Width, GameSystem.MainForm.Height),
            };

            // 돌 생성 관련 변수들
            stoneGenCooltime = (Second)0.5f;
            stoneGenTimer = Second.Zero;

            // System.Random 객체
            random = new Random();

            // 상태를 초기화 된 상태로 변경한다.
            status = GameStatus.Initialized;
        }
        #endregion
        #region overrided function
        /// <summary>
        /// 게임 루프가 시작되기 전에 한번 호출되는 함수
        /// </summary>
        protected override void OnInitialize()
        {
            base.OnInitialize();
            stoneList.Clear();
            stoneGenTimer = Second.Zero;
        }
        /// <summary>
        /// 게임 루프가 시작된 후 매 프레임 호출되는 함수
        /// </summary>
        /// <param name="deltaTime"></param>
        public override void Update(Second deltaTime)
        {
            switch (status)
            {
                // 게임이 아직 시작하지 않았을 때
                case GameStatus.Initialized:
                    if (InputHelper.IsPressed(Key.Space))
                        status = GameStatus.Playing;
                    break;

                // 게임이 굴러가고 있을 때
                case GameStatus.Playing:
                    // 돌을 만든다.
                    GenerateStone(deltaTime);

                    // 플레이어와 돌도 update한다
                    player.Update(deltaTime);
                    foreach (Stone stone in stoneList)
                        stone.Update(deltaTime);

                    // 돌과 플레이어가 충돌했을 경우는 플레이어를 죽도록 한다.
                    foreach (Stone stone in stoneList)
                        if (player.Collider.IsCollided(stone.Collider) == true)
                        {
                            status = GameStatus.Dead;
                            break;
                        }

                    // 화면을 벗어난 돌들은 stoneList 변수에서 제거한다.
                    for (int i = 0; i < stoneList.Count; i++)
                        if (stoneList[i].Position.Y < -100)
                        {
                            stoneList.RemoveAt(i);
                            i--;
                        }
                    break;

                // 플레이어가 죽었을 때
                case GameStatus.Dead:
                    break;

                default:
                    throw new ArgumentException();
            }
        }
        /// <summary>
        /// 게임 루프가 시작된 후 매 프레임 호출되는 함수.
        /// 렌더링을 조절한다.
        /// </summary>
        /// <param name="deltaTime"></param>
        public override void Render(Second deltaTime)
        {
            backgroundSprite.Render(ref cam.Matrix);
            player.Render(cam);
            foreach (Stone e in stoneList)
                e.Render(cam);

            switch (status)
            {
                case GameStatus.Initialized:
                    startLabel.Render(ref cam.Matrix);
                    break;
            }
        }
        /// <summary>
        /// 게임 루프가 끝나기 전에 호출되는 함수.
        /// </summary>
        protected override void OnStop()
        {
            base.OnStop();
        }
        /// <summary>
        /// 게임 루프가 끝난 후에 호출되는 함수.
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();
        }
        #endregion
        #region helper functions
        /// <summary>
        /// deltaTime 시간이 지난 상황에서 추가적으로 돌을 만드는 함수.
        /// </summary>
        /// <param name="deltaTime">이전 함수 호출로부터 지난 시간</param>
        public void GenerateStone(Second deltaTime)
        {
            stoneGenTimer += deltaTime;
            while (stoneGenTimer > stoneGenCooltime)
            {
                stoneGenTimer -= stoneGenCooltime;

                float randomX = random.NextFloat(0, GameSystem.MainForm.Width);
                Stone stone = new Stone(new Vector2(randomX, GameSystem.MainForm.Height), new Vector2(0, -100));
                stoneList.Add(stone);
            }
        }
        #endregion
    }
}
