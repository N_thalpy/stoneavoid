﻿using OpenTK;
using OpenTK.Input;
using System;
using Tri;
using Tri.Physics.Collider;
using Tri.Rendering;
using Tri.Rendering.Camera;
using Tri.SystemComponent;
using Tri.UI;

namespace StoneAvoid.FieldObject
{
    /// <summary>
    /// 플레이어 클래스
    /// </summary>
    public class Player
    {
        #region enum
        /// <summary>
        /// 플레이어의 상태
        /// </summary>
        private enum PlayerStatus
        {
            /// <summary>
            /// 기본값
            /// </summary>
            None,

            /// <summary>
            /// 왼쪽을 보면서 서있음
            /// </summary>
            IdleLeft,
            /// <summary>
            /// 오른쪽을 보면서 서있음
            /// </summary>
            IdleRight,

            /// <summary>
            /// 왼쪽을 보면서 달림
            /// </summary>
            RunLeft,
            /// <summary>
            /// 오른쪽을 보면서 달림
            /// </summary>
            RunRight,
        }
        #endregion

        #region private variables
        /// <summary>
        /// 플레이어의 상태.
        /// 플레이어의 상태에 따라서 적당히 스프라이트를 바꾼다.
        /// </summary>
        private PlayerStatus playerStatus
        {
            get
            {
                return _playerStatus;
            }
            set
            {
                if (_playerStatus == value)
                    return;

                _playerStatus = value;
                if (currentSprite != null)
                {
                    currentSprite.Size = new Vector2(playerSize.X, playerSize.Y);
                    currentSprite.Reset();
                }

                switch (value)
                {
                    case PlayerStatus.IdleLeft:
                        currentSprite = idleSprite;
                        currentSprite.Size = new Vector2(-playerSize.X, playerSize.Y);
                        break;

                    case PlayerStatus.IdleRight:
                        currentSprite = idleSprite;
                        currentSprite.Size = new Vector2(playerSize.X, playerSize.Y);
                        break;

                    case PlayerStatus.RunLeft:
                        currentSprite = runSprite;
                        currentSprite.Size = new Vector2(-playerSize.X, playerSize.Y);
                        break;

                    case PlayerStatus.RunRight:
                        currentSprite = runSprite;
                        currentSprite.Size = new Vector2(playerSize.X, playerSize.Y);
                        break;

                    default:
                        throw new ArgumentException();
                }
            }
        }
        private PlayerStatus _playerStatus;

        /// <summary>
        /// 플레이어의 위치
        /// </summary>
        private Vector2 position;

        /// <summary>
        /// 현재 스프라이트
        /// </summary>
        private AnimatedSprite currentSprite;
        /// <summary>
        /// 달릴 때의 스프라이트
        /// </summary>
        private readonly static AnimatedSprite runSprite;
        /// <summary>
        /// 서있을 때의 스프라이트
        /// </summary>
        private readonly static AnimatedSprite idleSprite;
        
        /// <summary>
        /// 플레이어의 크기
        /// </summary>
        private readonly static Vector2 playerSize;
        /// <summary>
        /// 플레이어의 이동 속도
        /// </summary>
        private readonly static float playerSpeed;
        #endregion
        #region public variables
        /// <summary>
        /// 충돌 체크용 Collider
        /// </summary>
        public readonly SquareCollider Collider;
        #endregion

        #region static .ctor / .ctor
        /// <summary>
        /// static .ctor
        /// </summary>
        static Player()
        {
            playerSize = new Vector2(100, 100);
            playerSpeed = 250;

            idleSprite = new AnimatedSprite()
            {
                Depth = ZOrder.Player,
                Size = playerSize,
                Anchor = new Vector2(0.5f, 0),
                TextureList = new Texture[]
                {
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Idle_1.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Idle_2.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Idle_3.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Idle_4.png"),
                },
                FrameInterval = (Second)0.1f,
                Angle = Degree.Zero,
            };
            runSprite = new AnimatedSprite()
            {
                Depth = ZOrder.Player,
                Size = playerSize,
                Anchor = new Vector2(0.5f, 0),
                TextureList = new Texture[]
                {
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_1.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_2.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_3.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_4.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_5.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_6.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_7.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_8.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_9.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_10.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_11.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_12.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_13.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_14.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_15.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_16.png"),
                    Texture.CreateFromBitmapPath("Resources/UnityChan/Unitychan_Run_17.png"),
                },
                FrameInterval = (Second)0.1f,
                Angle = Degree.Zero,
            };
        }
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="position"></param>
        public Player(Vector2 position)
        {
            this.position = position;

            playerStatus = PlayerStatus.IdleLeft;
            currentSprite = idleSprite;
            currentSprite.Position = position;

            this.Collider = new SquareCollider(
                new Vector2(position.X, position.Y + playerSize.Y * 0.4f), 
                new Vector2(playerSize.X * 0.3f, playerSize.Y * 0.6f),
                Degree.Zero);
        }
        #endregion
        #region Update / Render
        /// <summary>
        /// 매 프레임 호출되는 함수
        /// </summary>
        /// <param name="deltaTime">프레임 사이의 시간</param>
        public void Update(Second deltaTime)
        {
            Vector2 moveVector = Vector2.Zero;
            if (InputHelper.IsDown(Key.Left))
                moveVector -= Vector2.UnitX;
            if (InputHelper.IsDown(Key.Right))
                moveVector += Vector2.UnitX;

            if (moveVector != Vector2.Zero)
            {
                if (moveVector.X > 0)
                    playerStatus = PlayerStatus.RunRight;
                else
                    playerStatus = PlayerStatus.RunLeft;
            }
            else
            {
                switch (playerStatus)
                {
                    case PlayerStatus.RunLeft:
                        playerStatus = PlayerStatus.IdleLeft;
                        break;

                    case PlayerStatus.RunRight:
                        playerStatus = PlayerStatus.IdleRight;
                        break;
                }
            }

            position += moveVector * playerSpeed * deltaTime;
            Collider.Velocity = moveVector * playerSpeed;

            Collider.Update(deltaTime);

            currentSprite.Position = position;
            currentSprite.Update(deltaTime);
        }
        /// <summary>
        /// 매 렌더링 프레임마다 호출되는 함수
        /// </summary>
        /// <param name="cam">카메라</param>
        public void Render(OrthographicCamera cam)
        {
            currentSprite.Render(ref cam.Matrix);
        }
        #endregion
    }
}
