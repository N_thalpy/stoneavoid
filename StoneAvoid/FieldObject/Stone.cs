﻿using OpenTK;
using Tri;
using Tri.Physics.Collider;
using Tri.Rendering;
using Tri.Rendering.Camera;
using Tri.UI;

namespace StoneAvoid.FieldObject
{
    public class Stone
    {
        public Vector2 Position;

        private Vector2 velocity;

        private readonly Sprite sprite;
        public readonly SquareCollider Collider;

        private readonly static Texture stoneTexutre;
        private readonly static Vector2 stoneSize;

        static Stone()
        {
            stoneTexutre = Texture.CreateFromBitmapPath("Resources/Stone.png");
            stoneSize = new Vector2(75, 75);
        }
        public Stone(Vector2 position, Vector2 initVelocity)
        {
            Position = position;
            sprite = new Sprite()
            {
                Position = position,
                Depth = ZOrder.Player,
                Size = stoneSize,
                Anchor = new Vector2(0.5f, 0.5f),
                Texture = stoneTexutre,
                Angle = Degree.Zero,
            };
            Collider = new SquareCollider(Position, stoneSize * 0.8f, Degree.Zero);

            this.velocity = initVelocity;
            Collider.Velocity = this.velocity;
        }

        public void Render(OrthographicCamera cam)
        {
            sprite.Render(ref cam.Matrix);
        }
        public void Update(Second deltaTime)
        {
            this.velocity -= 200f * deltaTime * Vector2.UnitY;
            Collider.Velocity = this.velocity;
            Position += velocity * deltaTime;

            sprite.Position = Position;
            Collider.Update(deltaTime);
        }
    }
}
